#pragma once

#include "GameBoard.h"
#include "BoardPoint.h"

#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <array>
#include <utility>

#undef max

class Pathfinder final {
public:
    explicit Pathfinder(const GameBoard& board)
        : board_(board) {
    }

    void Pathfind(const BoardPoint& from) {
        {
            for (size_t x = 0; x < board_.getMapSize(); ++x) {
                for (size_t y = 0; y < board_.getMapSize(); ++y) {
                    const auto& el = board_.getElementAt(x, y);
                    if (trivial_bads_.count(el)) {
                        bad_barriers_[BoardPoint{static_cast<int>(x), static_cast<int>(y)}] = true;
                    }
                    if (BoardElement::MEATCHOPPER == el) {
                        for (int dx = -1; dx < 2; ++dx) {
                            for (int dy = -1; dy < 2; ++dy) {
                                if (std::abs(dx) == std::abs(dy)) {
                                    continue;
                                }
                                const auto pt = BoardPoint{static_cast<int>(x + dx), static_cast<int>(y + dy)};
                                if (!(from == pt)) {
                                    bad_barriers_[pt] = true;
                                }
                            }
                        }
                    }
                }
            }

            GameBoard temp_board = board_;

            const auto build_bomp_map = [&](const BoardElement& bomb_type) {
                for (size_t x = 0; x < temp_board.getMapSize(); ++x) {
                    for (size_t y = 0; y < temp_board.getMapSize(); ++y) {
                        const auto& el = temp_board.getElementAt(x, y);
                        if (bomb_type == el) {
                            ProcessBomb(BoardPoint{static_cast<int>(x), static_cast<int>(y)}, temp_board);
                        }
                    }
                }
            };
            build_bomp_map(BoardElement::BOMB_TIMER_0);
            build_bomp_map(BoardElement::BOMB_TIMER_1);
            build_bomp_map(BoardElement::BOMB_TIMER_2);
            build_bomp_map(BoardElement::BOMB_TIMER_3);
            build_bomp_map(BoardElement::BOMB_TIMER_4);
            // build_bomp_map(BoardElement::BOMB_TIMER_5);

            for (size_t x = 0; x < temp_board.getMapSize(); ++x) {
                for (size_t y = 0; y < temp_board.getMapSize(); ++y) {
                    const auto& el = temp_board.getElementAt(x, y);
                    if (bomb_to_timer_.count(el)) {
                        const int timer = bomb_to_timer_.at(el);
                        const auto f = [&](int x, int y) {
                            bad_bombs_[BoardPointTime{x, y, timer}] = true;
                            under_bomb_.emplace(x, y);
                        };
                        DoInBombRange(BoardPoint{static_cast<int>(x), static_cast<int>(y)}, f);
                    }
                }
            }
        }

        const auto is_point_ok = [&](const BoardPointTime& p) {
            return !bad_barriers_[p] && !bad_bombs_[p];
        };

        std::queue<BoardPointTime> q;

        const auto start = BoardPointTime{from.getX(), from.getY(), 0};
        if (!is_point_ok(start)) {
            return;
        }
        parent_[start] = start;
        q.push(start);

        while (!q.empty()) {
            const auto t = q.front();
            q.pop();

            for (const auto& d : deltas_) {
                auto next = t + d;
                ++next.time;

                // clamp
                if (next.time > 6) {
                    next.time = 6;
                }

                if (!next.isOutOfBoard(board_.getMapSize()) &&
                    !parent_.count(next) && is_point_ok(next)) {
                    q.push(next);
                    parent_[next] = t;
                }
            }
        }
    }

    BoardPoint FirstPointTo(const BoardPoint& to) const {
        BoardPointTime it;
        for (int i = 0; i < 7; ++i) {
            BoardPointTime q{to.x, to.y, i};
            if (parent_.count(q)) {
                it = q;
                break;
            }
        }

        if (0 == it.getX() && 0 == it.getY()) {
            return BoardPoint{};
        }

        while (true) {
            const auto parent = parent_.at(it);
            if (parent_.at(parent) == parent) {
                break;
            }
            it = parent;
        }

        return it;
    }

    BoardPoint TrySave(const BoardPoint& from) {
        if (!bad_bombs_.count(BoardPointTime{ from.x, from.y, 1 })) {
            std::cout << "No save is required\n";
            return from;
        }

        const auto t = BoardPointTime{from.x, from.y, 0};
        for (const auto& d : deltas_) {
            auto next = t + d;
            ++next.time;

            if (!next.isOutOfBoard(board_.getMapSize()) &&
                !bad_bombs_.count(next) &&
                !trivial_bads_.count(board_.getElementAt(next))) {
                std::cout << "Yay\n";
                return next;
            }
        }

        std::cout << "Oops\n";
        return from;
    }

    size_t GetPointDist(const BoardPoint& to) const {
        BoardPointTime it;
        for (int i = 0; i < 7; ++i) {
            BoardPointTime q{ to.x, to.y, i };
            if (parent_.count(q)) {
                it = q;
                break;
            }
        }

        if (0 == it.getX() && 0 == it.getY()) {
            return std::numeric_limits<size_t>::max();
        }

        size_t dep = 0;
        while (true) {
            const auto parent = parent_.at(it);
            if (parent_.at(parent) == parent) {
                break;
            }
            ++dep;
            it = parent;
        }

        return dep;
    }

    bool IsUnderBomb(const BoardPoint& point) const {
        return under_bomb_.count(point);
    }

    std::vector<BoardPoint> GetReachablePoints(const BoardPoint& exclude) const {
        std::unordered_set<BoardPoint, BoardPointTime::PointHash> q;
        for (auto& x : parent_) {
            if (!(x.first == exclude)) {
                q.insert(x.first);
            }
        }

        std::vector<BoardPoint> r;
        for (auto& x : q) {
            r.push_back(x);
        }
        return r;
    }
private:
    class BoardPointTime : public BoardPoint {
     public:
         BoardPointTime()
             : BoardPoint{0, 0}
             , time{0} {
         }

        BoardPointTime(int x, int y, int time) 
            : BoardPoint{x, y}
            , time{time} {
        }

        BoardPointTime operator+(const BoardPoint& p) const {
            return BoardPointTime{p.x + x, p.y + y, time};
        }

        struct PointHash final {
            size_t operator()(const BoardPointTime& p) const {
                return (static_cast<size_t>(p.x) << 32) |
                    (static_cast<size_t>(p.y) << 16) |
                    p.time;
            }

            size_t operator()(const BoardPoint& p) const {
                return (static_cast<size_t>(p.x) << 32) |
                    (static_cast<size_t>(p.y) << 16);
            }
        };

        int time;
    };

    void ProcessBomb(const BoardPoint& point, GameBoard& board) {
        const auto el = board.getElementAt(point);
        const auto f = [&](int x, int y) {
            const auto p = BoardPoint{x, y};
            if (p.isOutOfBoard(board.getMapSize())) {
                return;
            }

            const auto curr = board.getElementAt(p);
            if (curr != el && bomb_to_timer_.count(curr)) {
                // std::cout << "Convert bomb from " << bomb_to_timer_.at(curr) << " to " << bomb_to_timer_.at(el) << "\n";
                board.setElementAt(x, y, el);
                ProcessBomb(p, board);
            }
        };

        DoInBombRange(point, f);
    }

    template<class Visitor>
    void DoInBombRange(const BoardPoint& point, Visitor f) {
        const int len = 4;
        for (int c = 1; c < len; ++c) {
            const int x = point.x;
            const int y = point.y + c;
            f(x, y);
            if (trivial_bads_.count(board_.getElementAt(x, y))) {
                break;
            }
        }
        for (int c = 1; c < len; ++c) {
            const int x = point.x + c;
            const int y = point.y;
            f(x, y);
            if (trivial_bads_.count(board_.getElementAt(x, y))) {
                break;
            }
        }
        for (int c = 1; c < len; ++c) {
            const int x = point.x;
            const int y = point.y - c;
            f(x, y);
            if (trivial_bads_.count(board_.getElementAt(x, y))) {
                break;
            }
        }
        for (int c = 1; c < len; ++c) {
            const int x = point.x - c;
            const int y = point.y;
            f(x, y);
            if (trivial_bads_.count(board_.getElementAt(x, y))) {
                break;
            }
        }
    }

    const std::array<BoardPoint, 4> deltas_ = {
        BoardPoint{0, 1}, BoardPoint{1, 0}, BoardPoint{0, -1}, BoardPoint{-1, 0}
    };
    std::unordered_map<BoardPointTime, BoardPointTime, BoardPointTime::PointHash> parent_;
    std::unordered_set<BoardPoint, BoardPointTime::PointHash> under_bomb_;
    std::unordered_map<BoardPointTime, bool, BoardPointTime::PointHash> bad_bombs_;
    std::unordered_map<BoardPoint, bool, BoardPointTime::PointHash> bad_barriers_;
    const GameBoard& board_;

    const std::unordered_map<BoardElement, int> bomb_to_timer_ = {
        {BoardElement::BOMB_TIMER_0, 0},
        {BoardElement::BOMB_TIMER_1, 1},
        {BoardElement::BOMB_TIMER_2, 2},
        {BoardElement::BOMB_TIMER_3, 3},
        {BoardElement::BOMB_TIMER_4, 4},
        {BoardElement::BOMB_TIMER_5, 5},
    };

    const std::unordered_set<BoardElement> trivial_bads_ = {
        BoardElement::OTHER_BOMBERMAN,
        BoardElement::OTHER_BOMB_BOMBERMAN,
        BoardElement::WALL,
        BoardElement::WALL_DESTROYABLE,
        BoardElement::MEATCHOPPER,
        BoardElement::BOMB_TIMER_0,
        BoardElement::BOMB_TIMER_1,
        BoardElement::BOMB_TIMER_2,
        BoardElement::BOMB_TIMER_3,
        BoardElement::BOMB_TIMER_4,
        BoardElement::BOMB_TIMER_5,
        BoardElement::BOOM
    };
};