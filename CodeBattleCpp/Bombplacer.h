#pragma once

#include "Pathfinder.h"

class Bombplacer final {
public:
    explicit Bombplacer(const GameBoard& board)
        : board_{board} {
    }

    bool IsGoodPlaceForBomb(const BoardPoint& bomb, const BoardPoint& future_player) const {
        auto temp_board = board_;
        for (size_t x = 0; x < temp_board.getMapSize(); ++x) {
            for (size_t y = 0; y < temp_board.getMapSize(); ++y) {
                const auto el = temp_board.getElementAt(x, y);
                if (downgrade_bomb_.count(el)) {
                    temp_board.setElementAt(x, y, downgrade_bomb_.at(el));
                }
            }
        }
        temp_board.setElementAt(bomb.x, bomb.y, BoardElement::BOMB_TIMER_4);

        Pathfinder pf{temp_board};
        pf.Pathfind(future_player);
        BoardPoint excl{-1, -1};
        const auto rp = pf.GetReachablePoints(excl);
        const auto size = [&] {
            int size = 0;
            for (const auto& x : rp) {
                if (!pf.IsUnderBomb(x)) {
                    ++size;
                }
            }
            return size;
        }();
        std::cout << size << "\n";
        return size > 1;
    }
private:
    const GameBoard& board_;

    const std::unordered_map<BoardElement, BoardElement> downgrade_bomb_ = {
        {BoardElement::BOMB_TIMER_1, BoardElement::BOMB_TIMER_0},
        {BoardElement::BOMB_TIMER_2, BoardElement::BOMB_TIMER_1},
        {BoardElement::BOMB_TIMER_3, BoardElement::BOMB_TIMER_2},
        {BoardElement::BOMB_TIMER_4, BoardElement::BOMB_TIMER_3},
        {BoardElement::BOMB_TIMER_5, BoardElement::BOMB_TIMER_4},
    };
};