﻿#include <iostream>
#include <algorithm>

#include "GameClient.h"
#include "Pathfinder.h"
#include "Bombplacer.h"

#undef max

std::pair<BoardPoint, size_t> first_in_line(const BoardPoint& me, std::vector<BoardPoint> others) {
    BoardPoint candidate;
    size_t distance = 1e9;
    do {
        BoardPoint prev = me;
        size_t currd = 0;
        for (const auto& pt : others) {
            currd += prev.DistanceM(pt);
            prev = pt;
        }
        if (currd < distance) {
            distance = currd;
            candidate = others.front();
        }
    } while (std::next_permutation(others.begin(), others.end()));

    return { candidate, distance };
}

template<class T, class Visitor>
void cnk_rec(const std::vector<T>& vec, const Visitor& func, int i, int level, int k, std::vector<T>& stack) {
    if (level == k) {
        func(stack);
        return;
    }

    for (int j = i + 1; j < vec.size(); ++j) {
        stack.push_back(vec[j]);
        cnk_rec(vec, func, j, level + 1, k, stack);
        stack.pop_back();
    }
}

template<class T, class Visitor>
void cnk(const std::vector<size_t>& aval_indexes, const std::vector<T>& vec, int k, const Visitor& func) {
    std::cout << "Doing cnk n=" << vec.size() << " k=" << k << "\n";
    std::vector<T> stack;
    for (const auto& i : aval_indexes) {
        stack.push_back(vec[i]);
        cnk_rec(vec, func, i, 1, k, stack);
        stack.pop_back();
    }
}

int main() {
    const auto global_start_time = std::chrono::high_resolution_clock::now();
    size_t deaths = 0;
    size_t saves = 0;
    bool last_save = false;
    std::unordered_map<BoardPoint, int, BoardPoint::PointHash> idling_players_map;

    // После регистрации на сайте вы попадаете на свою игровую доску. Вставьте URL из строки браузера для инициализации клиента. 
    // ВАЖНО: В строке должны быть обязательно ид игрока (в примере: 82bc5roztht315o8f5yc) и код (в примере: 3242242588940318227)
    GameClient* gcb = new GameClient("http://codebattle2020final.westeurope.cloudapp.azure.com/codenjoy-contest/board/player/1l6oih6kj2ftf5ab18bx?code=3408278105464959082&gameName=bomberman");
    gcb->Run([&]() {
        std::cout << "Begin\n";
        const auto start_time = std::chrono::high_resolution_clock::now();
        // Используем реализованную библиотеку, которая описывает игровую доску. Вы можете ее модифицировать по вашему смотрению.
        GameBoard* gb = gcb->get_GameBoard();
        if (gb->isMyBombermanDead()) {
            std::cout << "Isdead\n";
            ++deaths;
            if (last_save) {
                --saves;
            }
            gcb->Blank();
            return;
        }

        const auto me = gb->getBomberman().front();

        Pathfinder pathfinder{*gb};
        pathfinder.Pathfind(me);

        const auto idle_players_safe = [&] {
            for (auto& x : idling_players_map) {
                if (BoardElement::OTHER_BOMBERMAN != gb->getElementAt(x.first)) {
                    x.second = 0;
                }
            }
            const auto players = gb->getOtherBombermans();
            for (const auto& player : players) {
                ++idling_players_map[player];
            }

            std::vector<BoardPoint> idle_players;
            const int idling_threshold = 4;
            for (const auto& x : idling_players_map) {
                if (x.second > idling_threshold && !pathfinder.IsUnderBomb(x.first)) {
                    idle_players.push_back(x.first);
                }
            }
            return idle_players;
        }();

        BoardPoint to = [&] {
            const auto construct_points = [&](const BoardPoint& p) {
                std::vector<BoardPoint> points;
                const std::vector<BoardPoint> deltas = {
                    {1, 1}, {0, 2},
                    {-1, -1}, {0, -2},
                    {1, -1}, {2, 0},
                    {-1, 1}, {-2, 0},
                };
                for (const auto& d : deltas) {
                    const auto next = p + d;
                    if (!(me == next) &&
                        !next.isOutOfBoard(gb->getMapSize())
                        && !pathfinder.IsUnderBomb(next)) {
                        points.push_back(next);
                    }
                }
                return points;
            };

            std::vector<BoardPoint> targets;
            targets.insert(std::end(targets), std::begin(idle_players_safe), std::end(idle_players_safe));
            const auto choppers = gb->getMeatChoppers();
            targets.insert(std::end(targets), std::begin(choppers), std::end(choppers));

            const auto construct_result = [&](const BoardPoint& from) {
                BoardPoint best;
                const auto points_around = construct_points(from);
                size_t dist = std::numeric_limits<size_t>::max();
                for (const auto& z : points_around) {
                    const auto c = pathfinder.GetPointDist(z);
                    if (c < dist) {
                        dist = c;
                        best = z;
                    }
                }
                return std::make_pair(best, dist);
            };

            std::vector<size_t> aval_indexes;
            for (size_t i = 0; i < targets.size(); ++i) {
                if (construct_result(targets[i]).second > 1000) {
                    continue;
                }
                aval_indexes.push_back(i);
            }

            size_t currd = 1e9;
            BoardPoint best_meat;
            cnk(aval_indexes, targets, 4, [&] (const std::vector<BoardPoint>& result) {
                const auto curr = first_in_line(me, result);
                if (curr.second < currd) {
                    currd = curr.second;
                    best_meat = curr.first;
                }
            });

            return construct_result(best_meat).first;
        }();
        std::cout << "Finding path to " << to.x << " " << to.y << " from " << me.x << " " << me.y << "\n"; 
        to = pathfinder.FirstPointTo(to);

        Bombplacer bp{*gb};
        const auto action = bp.IsGoodPlaceForBomb(me, to) ? TurnAction::BeforeTurn : TurnAction::None;

        const auto go_to = [&me, &gcb](const BoardPoint& target, const TurnAction& action) {
            if (target.x > me.x) {
                gcb->Down(action);
            }
            else if (target.x < me.x) {
                gcb->Up(action);
            }
            else if (target.y > me.y) {
                gcb->Right(action);
            }
            else if (target.y < me.y) {
                gcb->Left(action);
            }
            else {
                gcb->Blank();
            }
        };

        last_save = false;
        if (0 == to.x && 0 == to.y) {
            std::cout << "IDK!\n";
            // stage 1
            const auto stage_1 = [&] {
                BoardPoint excl{ -1, -1 };
                const auto rp = pathfinder.GetReachablePoints(excl);
                for (const auto& x : rp) {
                    if (!pathfinder.IsUnderBomb(x)) {
                        go_to(pathfinder.FirstPointTo(x), TurnAction::None);
                        return true;
                    }
                }
                return false;
            }();
            // stage 2
            if (!stage_1) {
                const auto save = pathfinder.TrySave(me);
                if (!(save == me)) {
                    ++saves;
                    last_save = true;
                }
                go_to(save, TurnAction::None);
            }
            else {
                std::cout << "save stage 1 ok\n";
            }
        } else {
            go_to(to, action);
        }

        //
        std::cout << "Time spent: " << std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - start_time).count() << "ms\n";
        std::cout << "Running " << std::chrono::duration_cast<std::chrono::minutes>(
            std::chrono::high_resolution_clock::now() - global_start_time).count() << "m with " << deaths << " deaths\n";
        std::cout << "saves: " << saves << "\n";
    });

    getchar();
    return 0;
}
