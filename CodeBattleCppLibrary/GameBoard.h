﻿#pragma once

#include "BoardElement.h"
#include "BoardPoint.h"
#include <list>
#include <vector>

class GameBoard
{
public:
    GameBoard(BoardElement** map, int map_size);
    bool hasElementAt(BoardPoint point, BoardElement element);
    BoardElement getElementAt(BoardPoint point) const;
    BoardElement getElementAt(int x, int y) const;
    void setElementAt(int x, int y, BoardElement el);
    std::list<BoardPoint> findAllElements(BoardElement element) const;
    std::list<BoardPoint> getWalls();
    std::list<BoardPoint> getDestroyableWalls();
    std::list<BoardPoint> getMeatChoppers();
    std::list<BoardPoint> getBomberman();
    std::list<BoardPoint> getDeadBomberman();
    std::list<BoardPoint> getOtherBombermans();
    std::list<BoardPoint> getOtherDeadBombermans();
    std::list<BoardPoint> getOtherBombermanBombs();
    std::list<BoardPoint> getBombs();
    std::list<BoardPoint> getBlasts();
    bool isBarrierAt(BoardPoint point);
    bool isMyBombermanDead();
    std::list<BoardPoint> getBarriers() const;
    void printBoard();
    int getMapSize() const;
    ~GameBoard();

private:
    std::vector<std::vector<BoardElement>> map;
    int map_size;
};