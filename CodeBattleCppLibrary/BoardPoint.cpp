#include "BoardPoint.h"
#include <iostream>

BoardPoint::BoardPoint(int x, int y)
{
    this->x = x;
    this->y = y;
}
BoardPoint::BoardPoint()
{
    this->x = 0;
    this->y = 0;
}
BoardPoint::~BoardPoint() {

}
BoardPoint BoardPoint::shiftLeft(int delta)
{
    return BoardPoint(x - 1, y);
}

BoardPoint BoardPoint::shiftLeft() {
    return shiftLeft(1);
}
BoardPoint BoardPoint::shiftRight(int delta) {
    return BoardPoint(x + delta, y);
}
BoardPoint BoardPoint::shiftRight() {
    return shiftRight(1);
}
BoardPoint BoardPoint::shiftBottom(int delta) {
    return BoardPoint(x, y + delta);
}
BoardPoint BoardPoint::shiftBottom() {
    return shiftBottom(1);
}
BoardPoint BoardPoint::shiftTop(int delta) {
    return BoardPoint(x, y - delta);
}
BoardPoint BoardPoint::shiftTop() {
    return shiftTop(1);
}
int BoardPoint::getX() const {
    return this->x;
}
int BoardPoint::getY() const {
    return this->y;
}
bool BoardPoint::isOutOfBoard(int size) const {
    return x >= size || y >= size || x < 0 || y < 0;
}
void BoardPoint::print() {
    std::cout << "[" << x << "," << y << "]" << "\n";
}

bool operator==(const BoardPoint& leftPoint, const BoardPoint& rightPoint) {
    return leftPoint.x == rightPoint.x && leftPoint.y == rightPoint.y;
};
