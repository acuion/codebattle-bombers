#pragma once

#include <algorithm>

class BoardPoint
{
public:
    BoardPoint(int x, int y);
    BoardPoint();
    ~BoardPoint();

    BoardPoint shiftLeft(int delta);
    BoardPoint shiftLeft();
    BoardPoint shiftRight(int delta);
    BoardPoint shiftRight();
    BoardPoint shiftBottom(int delta);
    BoardPoint shiftBottom();
    BoardPoint shiftTop(int delta);
    BoardPoint shiftTop();
    friend bool operator==(const BoardPoint& leftPoint, const BoardPoint& rightPoint);
    BoardPoint operator+(const BoardPoint& rightPoint) const {
        return BoardPoint{ x + rightPoint.x, y + rightPoint.y };
    }
    bool operator<(const BoardPoint& rightPoint) const {
        if (x != rightPoint.x) {
            return x < rightPoint.x;
        }
        return y < rightPoint.y;
    }
    int getX() const;
    int getY() const;
    bool isOutOfBoard(int size) const;
    void print();

    double DistanceSq(const BoardPoint& pt) const {
        return (x - pt.x) * (x - pt.x) + (y - pt.y) * (y - pt.y);
    }

    size_t DistanceM(const BoardPoint& pt) const {
        return std::abs(x - pt.x) + std::abs(y - pt.y);
    }

    int x;
    int y;

    struct PointHash final {
        size_t operator()(const BoardPoint& p) const {
            return (static_cast<size_t>(p.x) << 32) |
                (static_cast<size_t>(p.y) << 16);
        }
    };
};